import React, { useState } from 'react';

const SearchBar = props => {
	const [term, setTerm] = useState('');

	const handleFormSubmit = e => {
		e.preventDefault();
		props.onTermSubmit(term);
	};

	return (
		<div className='ui segment search-bar'>
			<form className='ui form' onSubmit={handleFormSubmit}>
				<div className='field'>
					<label htmlFor='search'>Video Search</label>
					<input
						type='text'
						name='search'
						id='search'
						value={term}
						onChange={e => setTerm(e.target.value)}
					/>
				</div>
			</form>
		</div>
	);
};

export default SearchBar;
